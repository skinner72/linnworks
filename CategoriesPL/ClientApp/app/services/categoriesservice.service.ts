﻿import { Injectable, Inject} from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod, HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import { Category } from '../models/category.model';

@Injectable()

export class CategoriesService {

    categoriesAPI: string = "http://localhost:50192/";
    _baseUrl: string = "";
    constructor(private http: Http, @Inject('BASE_URL') baseUrl: string) {
        
        this._baseUrl = baseUrl;
    }

    getCategories(authToken, requestedService): Observable<Category[]> {

        var body = JSON.stringify(authToken, requestedService);
        var headerOptions = new Headers({ 'Content-Type': 'application/json'});
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });

        return this.http.post(this._baseUrl + 'api/Categories/', body, requestOptions)
            .map((res => res.json())).catch(this.errorHandler);
    }

    errorHandler(error: Response) {
        console.log(error);
        return Observable.throw(error);
    }

}