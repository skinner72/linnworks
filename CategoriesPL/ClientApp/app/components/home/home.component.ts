import { Component, OnDestroy  } from '@angular/core';
import { NgForm, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';  
import { CategoriesService } from '../../services/categoriesservice.service'  
import { HttpModule, Headers } from '@angular/http';
import { Category } from '../../models/category.model';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    providers: [CategoriesService,HttpModule],
})
    
export class HomeComponent {

    categoriesForm: FormGroup;
    requestedService: string = "LINNWORKS"
    errorMessage: any;
    public categoriesList: Category[];
    constructor(private _fb: FormBuilder, private _avRoute: ActivatedRoute, private _categoriesService: CategoriesService, private _router: Router) {

    }

    ngOnInit() {
        this.categoriesForm = new FormGroup({
            AuthToken: new FormControl()
        });

        this.categoriesForm = this._fb.group({

            AuthToken: ['', [Validators.required]]

        })
    }

    request() {

        if (!this.categoriesForm.valid) {

            return;

        }
        else {
            debugger;
            this._categoriesService.getCategories(this.categoriesForm.value, this.requestedService).subscribe(
                res => {
                    this.categoriesList = res;

                },
                err => {
                    console.error(err);
                }
            );
        }

    }  

}
