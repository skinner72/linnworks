﻿import { Deserializable } from "./deserializable.model";

export class Category implements Deserializable {
    categoryId: string;
    categoryName: string;
    count: number;

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }

}
