﻿import { Category } from "./category.model";

export interface Deserializable {
    deserialize(input: any): this;
}