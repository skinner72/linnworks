﻿using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CategoriesAPI.Services
{
    public class ServiceResolver : NinjectModule
    {
        private static string ServiceProvider { get; set; }
        private static int APIVersion { get; set; }

        public static T GetServiceBinding<T>(string serviceProvider, int apiVersion)
        {
            APIVersion = apiVersion;
            ServiceProvider = !String.IsNullOrEmpty(serviceProvider) ? serviceProvider.ToLower() : "linnworks";
            ICategoryService categoryservice;
            Ninject.IKernel kernel = new StandardKernel();
            kernel.Load(System.Reflection.Assembly.GetExecutingAssembly());
            T output = default(T);

            // Resolve the API version and implementation version
            switch (APIVersion)
            {
                case 1:
                    categoryservice = (ICategoryService)kernel.Get<ICategoryService>();
                    output = (T)categoryservice;
                    break;
                default:
                    categoryservice = (ICategoryService)kernel.Get<ICategoryService>();
                    output = (T)categoryservice;
                    break;
            }

            return output;
        }

        public override void Load()
        {
            switch (ServiceProvider)
            {
                case "linnworks":
                    Bind<ICategoryService>().To<CategoriesAPI.Services.LINNWORKS.CategoryService>();
                    break;
                default:
                    Bind<ICategoryService>().To<CategoriesAPI.Services.LINNWORKS.CategoryService>();
                    break;
            }

        }

    }
}
