﻿using CategoriesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CategoriesAPI.Services
{
    interface ICategoryService
    {
        CategoryResponseModel GetAllProductCategories(CategoryRequestModel input);
    }
}
