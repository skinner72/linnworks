﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CategoriesAPI.Models;
using CategoriesAPI.Repository;

namespace CategoriesAPI.Services.LINNWORKS
{
    public class CategoryService : ICategoryService
    {

        public CategoryResponseModel GetAllProductCategories(CategoryRequestModel input)
        {
            CategoryResponseModel output = new CategoryResponseModel();
            output = LinnworksRepository.GetAllProductCategories(input);

            return output;
        }


    }
}
