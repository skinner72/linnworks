﻿using CategoriesAPI.Models;
using Microsoft.Rest;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace CategoriesAPI.Repository
{
    public class LinnworksRepository
    {
        public static CategoryResponseModel GetAllProductCategories(CategoryRequestModel input)
        {
            CategoryResponseModel output = new CategoryResponseModel();

            using (var linnworksClient = new HttpClient())
            {
                string serviceUrl = "https://eu-ext.linnworks.net/api/Inventory/";
                string authToken = input.AuthToken; 
                string endpointCategories = "GetCategories";
                linnworksClient.BaseAddress = new Uri(serviceUrl);
                linnworksClient.DefaultRequestHeaders.Accept.Clear();
                linnworksClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                linnworksClient.DefaultRequestHeaders.Host = "eu.linnworks.net";
                linnworksClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authToken);
                StringContent jsonStringInput = new StringContent(String.Empty);
                HttpResponseMessage categoriesResponse = linnworksClient.PostAsync(endpointCategories, jsonStringInput).Result;
                string categories = String.Empty;
                if (categoriesResponse.IsSuccessStatusCode)
                {
                    categories = categoriesResponse.Content.ReadAsStringAsync().Result;

                    if (!String.IsNullOrEmpty(categories))
                    {
                        output.Categories = AssignNumberOfProductsToCategories(JsonConvert.DeserializeObject<List<CategoryModel>>(categories), authToken);

                    }
                    else
                    {


                    }

                }


                return output;
            }

        }

        private static List<CategoryModel> AssignNumberOfProductsToCategories(List<CategoryModel> categories, string authToken)
        {
            List<CategoryModel> updatedCategories = categories;
            using (var linnworksClient = new HttpClient())
            {
                string serviceUrl = "https://eu.linnworks.net/api/Dashboards/";
                string endpointCustomQuery = "ExecuteCustomScriptQuery?script=SELECT P.Categoryid, COUNT(*) FROM StockItem AS S INNER JOIN ProductCategories AS P ON P.Categoryid = S.Categoryid GROUP BY P.Categoryid";
                linnworksClient.BaseAddress = new Uri(serviceUrl);
                linnworksClient.DefaultRequestHeaders.Accept.Clear();
                linnworksClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                linnworksClient.DefaultRequestHeaders.Host = "eu.linnworks.net";
                linnworksClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authToken);
                
                //string script = "?script=SELECT P.Categoryid, COUNT(*) FROM StockItem AS S INNER JOIN ProductCategories AS P ON P.Categoryid = S.Categoryid GROUP BY P.Categoryid";
                
                StringContent customInput = new StringContent(String.Empty);

                HttpResponseMessage productsResponse = linnworksClient.PostAsync(endpointCustomQuery, customInput).Result;
                if (productsResponse.IsSuccessStatusCode)
                {
                    string rawResult = productsResponse.Content.ReadAsStringAsync().Result.Replace("\"\"", "Count");
                    string regexPattern = @"(?<=Results"":\[)(.*)(?=\])";
                    Regex resultRegex = new Regex(regexPattern);
                    string regexResult = resultRegex.Match(rawResult).Value;

                    StringBuilder extractedJsonResult = new StringBuilder();
                    extractedJsonResult.Append("[");
                    extractedJsonResult.Append(regexResult);
                    extractedJsonResult.Append("]");

                    List<CategoryModel> productCategories = JsonConvert.DeserializeObject<List<CategoryModel>>(extractedJsonResult.ToString());


                    foreach(CategoryModel category in categories)
                    {
                        category.Count = productCategories.FirstOrDefault(p => p.CategoryId == category.CategoryId) != null ? productCategories.FirstOrDefault(p => p.CategoryId == category.CategoryId).Count : 0;
                    }
                }

            }

            return updatedCategories;

        }




    }
}
