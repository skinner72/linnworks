﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CategoriesAPI.Models
{
    public class RequestModel
    {
        public string AuthToken { get; set; }
        public string RequestedService  { get; set; }
    }
}
