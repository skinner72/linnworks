﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CategoriesAPI.Models
{
    public class CategoryResponseModel: ResponseModel
    {
        public List<CategoryModel> Categories { get; set; }
    }
}
