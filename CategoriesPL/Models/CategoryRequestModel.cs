﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CategoriesAPI.Models
{
    public class CategoryRequestModel: RequestModel
    {
        public String ServiceProvider { get; set; }
    }
}
