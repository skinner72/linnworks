﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CategoriesAPI.Models
{
    public class ResponseModel
    {
        public string ResponseStatus { get; set; }
        public string ResponseMessage { get; set; }
    }
}
