﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace CategoriesAPI.Models
{
    public class CategoryModel
    {
        public String CategoryId { get; set; }

        public String CategoryName { get; set; }

        public int Count { get; set; }
    }
}
