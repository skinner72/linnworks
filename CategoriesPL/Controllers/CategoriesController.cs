﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CategoriesAPI.Models;
using CategoriesAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using System.Net;
using System.Net.Http;
using Microsoft.AspNetCore.Server.Kestrel.Internal.System.Collections.Sequences;
using Newtonsoft.Json;

namespace CategoriesAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Categories")]
    public class CategoriesController : Controller
    {
        private ICategoryService CategoryService;

        // POST: api/Categories
        [HttpPost]
        public JsonResult Post([FromBody]RequestModel requestInput)
        {
            CategoryResponseModel apiOutput = new CategoryResponseModel();
 
            try
            {
                if (requestInput != null && !String.IsNullOrEmpty(requestInput.AuthToken))
                {
                    MethodInfo mi = typeof(ServiceResolver).GetMethod("GetServiceBinding");
                    MethodInfo miConstructed = mi.MakeGenericMethod(typeof(ICategoryService));

                    string serviceProvider = requestInput.AuthToken;
                    int apiVersion = 1;

                    object[] args = { serviceProvider, apiVersion };
                    CategoryService = (ICategoryService)miConstructed.Invoke(apiOutput, args);

                    CategoryRequestModel input = new CategoryRequestModel();
                    input.ServiceProvider = serviceProvider;
                    input.AuthToken = requestInput.AuthToken;

                    apiOutput = CategoryService.GetAllProductCategories(input);

                }
                else
                {
                    apiOutput.ResponseStatus = HttpStatusCode.BadRequest.ToString();
                    apiOutput.ResponseMessage = "Missing required token";

                    return new JsonResult(apiOutput);
                }
            }
            catch(Exception ex)
            {
                apiOutput.ResponseStatus = HttpStatusCode.InternalServerError.ToString();
                apiOutput.ResponseMessage = ex.Message;

                return new JsonResult(apiOutput);
            }
            

            return new JsonResult(apiOutput.Categories);
        }

    }
}
